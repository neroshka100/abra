from django.contrib import admin
from django.urls import path, include
from abra import urls



urlpatterns = [
    path('admin/', admin.site.urls),    
    path('', include('abra.urls')),
]
