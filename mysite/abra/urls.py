import imp
from django.db import router
from rest_framework.routers import DefaultRouter
from .views import MessagesViewSet, MessagesList
from django.urls import path, include
from abra import views


router=DefaultRouter()
router.register('messages', MessagesViewSet)

urlpatterns=[
    path('api/',include(router.urls)),
    path('api/message/<str:sender>/', MessagesList.as_view())
]

