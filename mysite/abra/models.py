import datetime
from django.db import models
from django.utils import timezone

class Messages(models.Model):
    sender = models.CharField(max_length=20)
    receiver = models.CharField(max_length=20)
    message = models.TextField(max_length=500)
    subject= models.CharField(max_length=10)
    creation_date= models.DateTimeField('date published')
    def __str__(self):
        return self.subject
    


    